package com.rikkei.tranning.activity;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class FragmentF2 extends Fragment {
    Button btnFragment2;
    MediaPlayer mp3;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Fragment Lifecycle","onCreate F2");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_f2_layout, container, false);
        btnFragment2 = view.findViewById(R.id.btnFragment);
        Intent intent = getActivity().getIntent();
        mp3 = MediaPlayer.create(getActivity(), R.raw.test);
        mp3.start();

        final int timeCurrent = intent.getIntExtra("timeline", 0);

        btnFragment2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp3.stop();
                Intent data = new Intent();
                data.putExtra("timelineA", timeCurrent);
                getActivity().setResult(Activity.RESULT_OK, data);
                getActivity().finish();
            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("Fragment Lifecycle","onStart F2");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("Fragment Lifecycle","onResume F2");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("Fragment Lifecycle","onPause F2");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("Fragment Lifecycle","onStop F2");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("Fragment Lifecycle","onDestroy F2");
    }
}
