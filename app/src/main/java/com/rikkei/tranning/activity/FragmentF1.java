package com.rikkei.tranning.activity;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class FragmentF1 extends Fragment {
    Button btnFragment1;
    MediaPlayer mp3;
    int timeCurrentF1 = 0;
    int MY_CODE = 100;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Fragment Lifecycle","onCreate F1");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_f1_layout, container, false);
        btnFragment1 = view.findViewById(R.id.btnFragment);
        mp3 = MediaPlayer.create(getActivity(), R.raw.test);
        mp3.start();

        btnFragment1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp3.pause();
                Log.d("Fragment", "view button click");
                Intent intent = new Intent(getActivity(), Main2Activity.class);
                timeCurrentF1 = mp3.getCurrentPosition();
                intent.putExtra("timeline", timeCurrentF1);
                startActivityForResult(intent, MY_CODE);
            }
        });
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if ( requestCode == MY_CODE && resultCode == Activity.RESULT_OK && data != null ){
            timeCurrentF1 = data.getIntExtra("timelineA", 0);
            mp3.seekTo(timeCurrentF1);
            mp3.start();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("Fragment Lifecycle","onStart F1");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("Fragment Lifecycle","onResume F1");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("Fragment Lifecycle","onPause F1");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("Fragment Lifecycle","onStop F1");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("Fragment Lifecycle","onDestroy F1");
    }
}
